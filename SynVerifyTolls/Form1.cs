﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Configuration;
using System.Threading;
using System.Data.SqlClient;
using System.IO;
using System.Transactions;
using System.Diagnostics;

namespace SynVerifyTolls
{
    public partial class Form1 : Form
    {
        int seconds_time = Convert.ToInt32(ConfigurationManager.AppSettings["seconds_time"]);//获取实时同步的执行间隔时间
        string reportConnStr = ConfigurationManager.ConnectionStrings["sqlConnectionString_report"].ToString();//report
        string localConnStr = ConfigurationManager.ConnectionStrings["sqlConnectionString_local"].ToString();//fufuzhu
        string sql_share = ConfigurationManager.ConnectionStrings["sqlConnectionString_share"].ToString();
        string errorLogPath = System.Environment.CurrentDirectory + "\\errorlog.txt";//错误日志记录路径
        string successLogPath = System.Environment.CurrentDirectory + "\\successlog.txt";//成功日志记录路径
        System.Timers.Timer timer_clear = new System.Timers.Timer();
        int DBtype = Convert.ToInt32(ConfigurationManager.AppSettings["DBtype"]);

        public Form1()
        {
            InitializeComponent();
            seconds_time = seconds_time * 1000;//秒
        }

        private void button1_Click(object sender, EventArgs e)
        {
            button1.Enabled = false;
            //创建实时同步的线程
            Thread thread = new Thread(new ThreadStart(synBySeconds));
            thread.Start();
            timer_clear.AutoReset = false;
            timer_clear.Interval = 10000;
            timer_clear.Elapsed += new System.Timers.ElapsedEventHandler(clear_Elapse);
            timer_clear.Start();
        }

        #region 实时同步的方法
        void synBySeconds()
        {
            Thread thread = new Thread(new ThreadStart(syn_dt_lottery_orders));
            thread.Start();
        }

        void syn_dt_lottery_orders()
        {
            FillMsg("正在校驗...");
            string tablename = ConfigurationManager.AppSettings["verify_tableName"].ToString();
            int count = 0;
            string strLocal = "";
            string strAzure = "";
            int startHour = 0;
            int endHour = DateTime.Now.Hour;
            bool firstDone = false;
            string identityid = "";

            DateTime begindate = dateTimePicker1.Value.Date;
            if (begindate == DateTime.Now.Date)
            {
                while (true)
                {
                    if (DateTime.Now.DayOfWeek == DayOfWeek.Friday && DateTime.Now.Hour >= 3 && DateTime.Now.Hour < 6)
                    {
                        Thread.Sleep(1000 * 60 * 60 * 3);
                        continue;
                    }
                    count = 0;

                    try
                    {
                        if (firstDone == false)
                        {

                            for (startHour = 0; startHour < endHour; startHour++)
                            {
                                //strLocal = "select top 10000 id from [srv_lnk_report].dafacloud.dbo.dt_lottery_orders where openstate in (1,2) and add_time2>=DATEADD(hour, " + startHour +
                                //    ", Convert(varchar(100),getdate(),23)) and add_time2<DATEADD(hour, " + (startHour + 1) +
                                //    ", Convert(varchar(100),getdate(),23)) and id not in( select id from dt_lottery_proxy_verify) and flog=0 ";
                                //var localTable = SqlDbHelper.GetQuery(strLocal, LocalSqlParamterlocalConnStr);
                                //----------------------------------
                                List<SqlParameter> LocalSqlParamter = new List<SqlParameter>()
                                    {
                                        new SqlParameter("@datetimeStart",DateTime.Now.Date.AddHours(startHour)),
                                        new SqlParameter("@datetimeEnd",DateTime.Now.Date.AddHours(startHour+1))
                                    };
                                string strsql = "select identityid from dt_tenant where DBtype='" + DBtype + "' order by identityid";//获取所有的站长
                                var identitytable = SqlDbHelper.GetQuery(strsql, sql_share);
                                for (int i = 0; i < identitytable.Rows.Count; i++)
                                {
                                    identityid = identitytable.Rows[i]["identityid"].ToString();
                                    FillMsg("正在校验站长:" + identityid + "...");

                                    strLocal = "select top 10000 id from [srv_lnk_report].dafacloud.dbo.dt_lottery_orders where identityid='"+ identityid + "' and openstate in (1,2) and add_time2>=@datetimeStart " +
                                    " and add_time2<@datetimeEnd " +
                                    " and id not in( select id from dt_lottery_proxy_verify where identityid='" + identityid + "' and add_time2>=@datetimeStart and add_time2<@datetimeEnd) and flog=0 ";
                                    var localTable = SqlDbHelper.GetQuery(strLocal, LocalSqlParamter.ToArray(), localConnStr);
                                    //----------------------------------
                                    string id = "";
                                    for (int ii = 0; ii < localTable.Rows.Count; ii++)
                                    {
                                        id += localTable.Rows[ii]["id"];
                                        if (ii + 1 != localTable.Rows.Count)
                                            id += ",";
                                    }

                                    strAzure = "";
                                    if (id != "")
                                        strAzure = "select top 10000 id,identityid,user_id,lottery_code,normal_money,openState,add_time,SourceName from " + tablename + " where id in (" + id + ") ";
                                    else
                                    {
                                        FillMsg("dt_lottery_orders校驗時間點 : " + startHour + "~" + (startHour + 1) + "---" + DateTime.Now.ToString());
                                        WriteLogData("dt_lottery_orders校驗時間點 : " + startHour + "~" + (startHour + 1) + "---" + DateTime.Now.ToString());
                                        Thread.Sleep(1000);
                                        continue;
                                    }

                                    if (localTable.Rows.Count == 10000) //如果漏10000筆,需再次查詢一樣的時間區間
                                        startHour--;

                                    var table = SqlDbHelper.GetQuery(strAzure, null, reportConnStr);

                                    if (table.Rows.Count != 0)
                                    {
                                        count = table.Rows.Count;
                                        TimeSpan ts1 = new TimeSpan(DateTime.Now.Ticks);
                                        int result = SqlDbHelper.RunInsert(table, "isVerify", "dsp_lottery_proxyVerify_self", localConnStr);
                                        TimeSpan ts2 = new TimeSpan(DateTime.Now.Ticks);
                                        TimeSpan ts = ts1.Subtract(ts2).Duration();
                                        string dateDiff = ts.Seconds.ToString() + "秒";
                                        FillMsg("dt_lottery_orders成功補" + count + "条数据,耗时:" + dateDiff);
                                        WriteLogData("dt_lottery_orders成功補" + count + "条数据,耗时:" + dateDiff + "    同步汇总时间:" + DateTime.Now.ToString());
                                    }

                                }
                            }
                            firstDone = true;
                        }
                        else
                        {
                            //strLocal = "select top 10000 id from [srv_lnk_report].dafacloud.dbo.dt_lottery_orders where openstate in (1,2) and add_time2>=DATEADD(hour, -2, getdate()) " +
                            //    "and add_time2<DATEADD(minute, -1, getdate()) and id not in( select id from dt_lottery_proxy_verify) and flog=0 ";
                            //----------------------------------
                            List<SqlParameter> LocalSqlParamter = new List<SqlParameter>()
                                    {
                                        new SqlParameter("@datetimeStart",DateTime.Now.AddHours(-2)),
                                        new SqlParameter("@datetimeEnd",DateTime.Now.AddMinutes(-1)),
                                        new SqlParameter("@datetimeStart1",DateTime.Now.AddHours(-5))
                                    };
                            string strsql = "select identityid from dt_tenant where DBtype='" + DBtype + "' order by identityid";//获取所有的站长
                            var identitytable = SqlDbHelper.GetQuery(strsql, sql_share);
                            for (int i = 0; i < identitytable.Rows.Count; i++)
                            {
                                strLocal = "select top 10000 id from [srv_lnk_report].dafacloud.dbo.dt_lottery_orders where identityid='" + identityid + "' and openstate in (1,2) and add_time2>=@datetimeStart " +
                                "and add_time2<@datetimeEnd and id not in( select id from dt_lottery_proxy_verify where identityid='" + identityid + "' and add_time2>=@datetimeStart and add_time2<@datetimeEnd) and flog=0 ";
                                //----------------------------------
                                if (DateTime.Now.DayOfWeek == DayOfWeek.Friday && DateTime.Now.Hour >= 6 && DateTime.Now.Hour < 8)
                                {
                                    //strLocal = "select top 10000 id from [srv_lnk_report].dafacloud.dbo.dt_lottery_orders where openstate in (1,2) and add_time2>=DATEADD(hour, -5, getdate()) " +
                                    //    "and add_time2<DATEADD(minute, -1, getdate()) and id not in( select id from dt_lottery_proxy_verify) and flog=0 ";
                                    //----------------------------------
                                    strLocal = "select top 10000 id from [srv_lnk_report].dafacloud.dbo.dt_lottery_orders where identityid='" + identityid + "' and openstate in (1,2) and add_time2>=@datetimeStart1 " +
                                        "and add_time2<@datetimeEnd and id not in( select id from dt_lottery_proxy_verify where identityid='" + identityid + "' and add_time2>=@datetimeStart1 and add_time2<@datetimeEnd) and flog=0 ";
                                    //----------------------------------
                                }
                                //var localTable = SqlDbHelper.GetQuery(strLocal,localConnStr);
                                var localTable = SqlDbHelper.GetQuery(strLocal, LocalSqlParamter.ToArray(), localConnStr);
                                string id = "";
                                if (localTable != null)
                                {
                                    for (int ii = 0; ii < localTable.Rows.Count; ii++)
                                    {
                                        id += localTable.Rows[ii]["id"];
                                        if (ii + 1 != localTable.Rows.Count)
                                            id += ",";
                                    }
                                    strAzure = "";
                                    if (id != "")
                                        strAzure = "select top 10000 id,identityid,user_id,lottery_code,normal_money,openState,add_time,SourceName from " + tablename + " where  id in (" + id + ") ";
                                    else
                                    {
                                        FillMsg("dt_lottery_orders校驗時間點 : " + DateTime.Now.ToString());
                                        WriteLogData("dt_lottery_orders校驗時間點 : " + DateTime.Now.ToString());
                                        Thread.Sleep(1000 * 60 * 30);
                                        continue;
                                    }

                                    var table = SqlDbHelper.GetQuery(strAzure, null, reportConnStr);

                                    if (table.Rows.Count != 0)
                                    {
                                        count = table.Rows.Count;
                                        TimeSpan ts1 = new TimeSpan(DateTime.Now.Ticks);
                                        int result = SqlDbHelper.RunInsert(table, "isVerify", "dsp_lottery_proxyVerify_self", localConnStr);
                                        TimeSpan ts2 = new TimeSpan(DateTime.Now.Ticks);
                                        TimeSpan ts = ts1.Subtract(ts2).Duration();
                                        string dateDiff = ts.Seconds.ToString() + "秒";
                                        FillMsg("dt_lottery_orders成功補" + count + "条数据,耗时:" + dateDiff + "---" + DateTime.Now.ToString());
                                        WriteLogData("dt_lottery_orders成功補" + count + "条数据,耗时:" + dateDiff + "---" + DateTime.Now.ToString());
                                    }
                                }
                            }

                        }
                    }
                    catch (Exception ex)
                    {
                        if (ex.Message != "未将对象引用设置到对象的实例。")
                        {
                            FillErrorMsg(tablename + ":" + ex);
                            WriteErrorLog(tablename + ":" + DateTime.Now.ToString(), ex.ToString());
                        }
                    }
                    Thread.Sleep(1000 * 3);//睡眠时间
                }
            }
            else
            {
                try
                {
                    for (startHour = 0; startHour < 24; startHour++)
                    {
                        strLocal = "select top 10000 id from [srv_lnk_report].dafacloud.dbo.dt_lottery_orders where openstate in (1,2) and add_time2>=DATEADD(hour, " + startHour +
                            ", '" + begindate.ToString("yyyy-MM-dd") + "') and add_time2<DATEADD(hour, " + (startHour + 1) +
                            ", '" + begindate.ToString("yyyy-MM-dd") + "') and id not in( select id from dt_lottery_proxy_verify) and flog=0 ";

                        var localTable = SqlDbHelper.GetQuery(strLocal, localConnStr);
                        string id = "";
                        for (int ii = 0; ii < localTable.Rows.Count; ii++)
                        {
                            id += localTable.Rows[ii]["id"];
                            if (ii + 1 != localTable.Rows.Count)
                                id += ",";
                        }

                        strAzure = "";
                        if (id != "")
                            strAzure = "select top 10000 id,identityid,user_id,lottery_code,normal_money,openState,add_time,SourceName from " + tablename + " where  id in (" + id + ") ";
                        else
                        {
                            FillMsg("dt_lottery_orders校驗時間點 : " + startHour + "~" + (startHour + 1) + "---" + DateTime.Now.ToString());
                            WriteLogData("dt_lottery_orders校驗時間點 : " + startHour + "~" + (startHour + 1) + "---" + DateTime.Now.ToString());
                            Thread.Sleep(1000);
                            continue;
                        }

                        if (localTable.Rows.Count == 10000) //如果漏10000筆,需再次查詢一樣的時間區間
                            startHour--;

                        var table = SqlDbHelper.GetQuery(strAzure, null,reportConnStr);

                        if (table.Rows.Count != 0)
                        {
                            count = table.Rows.Count;
                            TimeSpan ts1 = new TimeSpan(DateTime.Now.Ticks);
                            int result = SqlDbHelper.RunInsert(table, "isVerify", "dsp_lottery_proxyVerify_self", localConnStr);
                            TimeSpan ts2 = new TimeSpan(DateTime.Now.Ticks);
                            TimeSpan ts = ts1.Subtract(ts2).Duration();
                            string dateDiff = ts.Seconds.ToString() + "秒";
                            FillMsg("dt_lottery_orders成功補" + count + "条数据,耗时:" + dateDiff + "---" + DateTime.Now.ToString());
                            WriteLogData("dt_lottery_orders成功補" + count + "条数据,耗时:" + dateDiff + "---" + DateTime.Now.ToString());
                        }
                    }
                }
                catch (Exception ex)
                {
                    if (ex.Message != "未将对象引用设置到对象的实例。")
                    {
                        FillErrorMsg(tablename + ":" + ex);
                        WriteErrorLog(tablename + ":" + DateTime.Now.ToString(), ex.ToString());
                    }
                }
            }
        }

        private byte[] StringConvertByte(string sqlstring)
        {
            string stringFromSQL = sqlstring;
            List<byte> byteList = new List<byte>();

            string hexPart = stringFromSQL.Substring(2);
            for (int i = 0; i < hexPart.Length / 2; i++)
            {
                string hexNumber = hexPart.Substring(i * 2, 2);
                byteList.Add((byte)Convert.ToInt32(hexNumber, 16));
            }

            byte[] original = byteList.ToArray();
            return original;
        }
        #endregion

        #region richTextBox记录
        private delegate void RichBox(string msg);
        private void FillMsg(string msg)
        {
            if (richTextBox1.InvokeRequired)
            {
                RichBox rb = new RichBox(FillMsg);
                richTextBox1.Invoke(rb, new object[] { msg });
            }
            else
            {
                if (richTextBox1.IsHandleCreated)
                {
                    richTextBox1.AppendText(msg);
                    richTextBox1.AppendText("\r\n");
                    richTextBox1.SelectionStart = richTextBox1.Text.Length;
                    richTextBox1.SelectionLength = 0;
                    richTextBox1.Focus();
                }
            }
        }

        private delegate void RichBoxErr(string msg);
        private void FillErrorMsg(string msg)
        {
            if (errorBox.InvokeRequired)
            {
                RichBoxErr rb = new RichBoxErr(FillErrorMsg);
                errorBox.Invoke(rb, new object[] { msg });
            }
            else
            {
                if (errorBox.IsHandleCreated)
                {
                    errorBox.AppendText(msg);
                    errorBox.AppendText("\r\n");
                    errorBox.SelectionStart = errorBox.Text.Length;
                    errorBox.SelectionLength = 0;
                    errorBox.Focus();
                }
            }
        }
        #endregion

        #region 打印成功日志记录
        private object obj1 = new object();
        public void WriteLogData(string msgex)
        {
            lock (obj1)
            {
                if (!File.Exists(successLogPath))
                {
                    FileStream fs1 = new FileStream(successLogPath, FileMode.Create, FileAccess.Write);//创建写入文件 
                    StreamWriter sw = new StreamWriter(fs1);
                    sw.Write(msgex);
                    sw.WriteLine();
                    sw.Close();
                    fs1.Close();
                }
                else
                {
                    FileStream fs = new FileStream(successLogPath, FileMode.Append, FileAccess.Write);
                    StreamWriter sr = new StreamWriter(fs);
                    sr.Write(msgex);
                    sr.WriteLine();
                    sr.Close();
                    fs.Close();
                }
            }
        }
        #endregion

        #region 打印错误日志记录
        private object obj = new object();
        public void WriteErrorLog(string msgex, string msgsql)
        {
            lock (obj)
            {
                if (!File.Exists(errorLogPath))
                {
                    FileStream fs1 = new FileStream(errorLogPath, FileMode.Create, FileAccess.Write);//创建写入文件 
                    StreamWriter sw = new StreamWriter(fs1);
                    sw.WriteLine(msgex);
                    sw.WriteLine(msgsql);
                    sw.WriteLine();
                    sw.WriteLine();
                    sw.Close();
                    fs1.Close();
                }
                else
                {
                    FileStream fs = new FileStream(errorLogPath, FileMode.Append, FileAccess.Write);
                    StreamWriter sr = new StreamWriter(fs);
                    sr.WriteLine(msgex);
                    sr.WriteLine(msgsql);
                    sr.WriteLine();
                    sr.WriteLine();
                    sr.Close();
                    fs.Close();
                }
            }
        }
        #endregion

        #region 清理textbox
        private void clear_Elapse(object sender, System.Timers.ElapsedEventArgs e)
        {
            try
            {
                if (System.DateTime.Now.ToString("mm") == "30")
                    ClearMsg();
            }
            catch (ThreadAbortException ex) { }
            catch (Exception ex2)
            {

            }
            finally
            {
                timer_clear.Start();
            }
        }
        private delegate void RichBoxClear();
        private void ClearMsg()
        {
            if (richTextBox1.InvokeRequired)
            {
                RichBoxClear rb = new RichBoxClear(ClearMsg);
                richTextBox1.Invoke(rb);
            }
            else
            {
                if (richTextBox1.IsHandleCreated)
                {
                    richTextBox1.Clear();
                }
            }
        }
        #endregion

        private void Form1_FormClosed(object sender, FormClosedEventArgs e)
        {
            Dispose();
            Application.Exit();
            System.Environment.Exit(0);
        }
    }
}
